## backup-wiki

Will backup my [dokuwiki](https://dokuwiki.org) install. Has the option to only back up the [recommended directories](https://www.dokuwiki.org/faq:backup) or the whole thing.

---

### How to use
* SSH to the server with dokuwiki
* Create a directory to hold the backups:
  * `mkdir ~/Backups`
* Initialize a borg repository: 
  * `borg init --encryption=repokey-blake2 ~/Backups`
* Create password file: 
  * `vim ~/.borg-pass`
  * Enter the password you used when you created the borg repository
  * Set permissions: `chmod 400 ~/.borg-pass`
* Configure the variables in the script
* Run the script and check for errors
* Schedule a cron job or systemd timer to run it daily.

---

### Links

* [Borg Backup](https://borgbackup.org)
* [Dokuwiki](https://dokuwiki.org)
