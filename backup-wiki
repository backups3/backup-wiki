#!/usr/bin/env bash

###########################################################
# Name : backup-wiki
# Desc : Backup Dokuwiki install
#
# Requirements:
#       - borgbackup
#
# Create a password file with password inside: 
# touch $HOME/.borg_pass
#
# Set permissions: 
# chmod 400 $HOME/.borg_pass
###########################################################
# Fail if pipeline returns non-zero status
set -e

###########################################################
# BORG VARIABLES
###########################################################
# Define Repository
export BORG_REPO="$HOME/Backups/"

# Define Passcommand to unlock repo
export BORG_PASSCOMMAND="cat $HOME/.borg_pass"

###########################################################
# DIRECTORY VARIABLES
###########################################################
# Define most important directories to backup 
# (Saves under 50M)
pages="/var/www/dokuwiki/data/pages"
meta="/var/www/dokuwiki/data/meta"
media="/var/www/dokuwiki/data/media"
media_meta="/var/www/dokuwiki/data/media_meta"
attic="/var/www/dokuwiki/data/attic"
media_attic="/var/www/dokuwiki/data/media_attic"
conf="/var/www/dokuwiki/conf"

# Array of backup directories
partial_wiki=( "$pages" "$meta" "$media" "$media_meta" "$attic" "$media_attic" "$conf" )
#backup_files="${partial_wiki[@]}"

# Define directorys to exclude
cache="/var/www/dokuwiki/data/cache"
index="/var/www/dokuwiki/index"
locks="/var/www/dokuwiki/data/locks"
tmp="/var/www/dokuwiki/tmp"

# Or just back it all up
whole_wiki="/var/www/dokuwiki"
backup_files="${whole_wiki}"

# Backup repository name
repo_name="{hostname}-{now:%Y-%m-%d}"

###########################################################
# FUNCTIONS
###########################################################
# Print information to screen
inform() { 
  printf "\n%s %s\n\n" "$( date )" "$*" >&2; 
}

# Message if backup was interrupted
trap 'echo $( date ) Backup interrupted >&2; exit2' INT TERM

###########################################################
# START BACKUP
###########################################################
# Exit if borg already running
if pidof -x borg >/dev/null; then
  printf "%s\n" "Backup already running"
  exit
fi

# Message that we're starting the backup
inform "Starting backup"

# Create Full Backup
borg create \
  --verbose \
  --list \
  --stats \
  --exclude-caches \
  --compression lz4 \
  --exclude "$cache" \
  --exclude "$index" \
  --exclude "$locks" \
  --exclude "$tmp" \
  ::"${repo_name}" \
  "${backup_files}"

# Define exit code of backup
backup_exit=$?

###########################################################
# PRUNE
###########################################################
# Message that we're pruning the repository
inform "Pruning repository"

# Maintain 7 daily, 4 weekly, and 6 monthly archives.
borg prune --verbose --list --prefix '{hostname}-' \
  --keep-daily 7 --keep-weekly 4 --keep-monthly 6

# Define exit code of prune
prune_exit=$?

###########################################################
# FINISH UP
###########################################################
# Use highest exit code as global exit code
global_exit=$(( backup_exit > prune_exit ? backup_exit : prune_exit ))

# Print status of completion
if [ ${global_exit} -eq 0 ]; then
  inform "Backup and Prune finished successfully"
elif [ ${global_exit} -eq 1 ]; then
  inform "Backup and/or Prune finished with warnings"
else
  inform "Backup and/or Prune finished with errors"
fi

# Exit
exit ${global_exit}
