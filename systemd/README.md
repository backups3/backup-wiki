## systemd

Here's a couple example systemd service/timer files

---

### How To

* Copy or create these files in `/etc/systemd/system/
* Enable the timer:
  * `sudo systemctl enable backup-wiki.timer`
* Start the timer:
  * `sudo systemctl start backup-wiki.timer`
